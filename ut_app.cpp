#include <assert.h>
#include "lib.hpp"


void test_square()
{
    assert(square(7) == 49);
}

int main()
{
    test_square();
    return 0;
}
