#include <stdio.h>
#include <stdlib.h>
#include <string.h>

size_t foo_len( const char *s )
{
  return strlen( s );
}

int main( int argc, char *argv[] )
{
  const char *a = nullptr;
  printf("size of a = %lu\n", foo_len(a) );
  return 0;
}
