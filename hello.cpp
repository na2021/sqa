#include <iostream>
#include <string>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;

int main()
{
    unsigned int n = 4;
    for (int i = 0; i < n ; ++i)
    {
        //std::cout<<"hello word "<< i << "\n"; //std::endl;
        printf("hello word %u\n", i);
        //fflush(stdout);
        std::this_thread::sleep_for(1s);
    }
    return 0;
}


