#include "Bar.hpp"

bool isBiggerThanTen(const int num)
{
    if (num >= 10)
    {
        return true;
    }
    else
    {
        return false;
    }
}

std::string fizzBazz(const int num)
{
    if (0 == num % 5 and 0 == num % 3)
    {
        return "fizzbazz";
    }
    else if (0 == num % 5)
    {
        return "fizz";
    }
    else if (0 == num % 3)
    {
        return "bazz";
    }
    else
    {
        return "";
    }
}
