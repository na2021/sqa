#include <gtest/gtest.h>

#include "Bar.hpp"

TEST(BarTest, isBiggerThanTenShouldReturnFalseIfGivenNumberIsSmallerThanTen)
{
    constexpr auto number = 5;
    ASSERT_FALSE(isBiggerThanTen(number));
}

TEST(BarTest, isBiggerThanTenShouldReturnTrueIfGivenNumberIsBiggerThanTen)
{
    constexpr auto number = 24;
    ASSERT_TRUE(isBiggerThanTen(number));
}

TEST(BarTest, isBiggerThanTenShouldReturnTrueIfGivenNumberIsEqualTen)
{
    constexpr auto number = 10u;
    ASSERT_TRUE(isBiggerThanTen(number));
}
