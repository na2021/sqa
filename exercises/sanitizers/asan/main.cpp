template <typename T>
class Matrix 
{
    T** data;
    unsigned width, height;
public:
    Matrix(unsigned width, unsigned height) : width(width), height(height)
    {
        data = new T*[height];
        for(int i=0; i < height; i++)
            data[i] = new T[width];
    }

    ~Matrix()
    {
        delete data;
    }
};

int main()
{
    Matrix<int> m(3,4);
    return 0;
}
