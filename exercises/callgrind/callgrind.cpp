#include <iostream>
#include <vector>

class A
{
private:
	std::vector<std::string> vec;
};

class B
{
private:
	std::vector<std::string> vec;
};


int countVectorItems(std::vector<A>& vec)
{
	int numberOfItems = 0;
	for (auto& x : vec)
	{
		++numberOfItems;
	}

	return numberOfItems;
}

bool isVectorBiggerThan100(std::vector<A>& vec)
{
	int numberOfItems = 0;
	for (auto x : vec)
	{
		++numberOfItems;
	}

	return numberOfItems > 100;
}


void incrementItterative(int & number)
{
	for (int i = 0; i < 10000; ++i)
	++number;
}

void incrementRecoursive(int & number)
{
	if ( number < 10000)
	{
	  ++number;
	  incrementRecoursive(number);
	}

	return;
}


int main(int argc, char *argv[])
{
	//int i = 0;
	//int j = 0;
	//incrementItterative(i);
	//incrementRecoursive(j);
 	
	
    	std::vector<A> vec(10000);

	if (isVectorBiggerThan100(vec))
	{
		std::cout << "Vector has " << countVectorItems(vec) << " elements";
	}
	
	return 0;
}



