#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <cstring>
#include <iostream>
using namespace std;

int sum(int *arr, int n);
int* getPrimes(int n);
bool isPrime(int x);

int main(int argc, char **argv) 
{
  int n = 10; 
  if(argc == 2)
  {
    n = atoi(argv[1]);
  }

  int *primes = getPrimes(n);

  long long unsigned int s = sum(primes, n);
  printf("The sum of the first %d primes is %llu\n", n, s);
  std::cout << "sum = " << s << std::endl ;
  delete [] primes;
  return 0;
}

int sum(int *arr, int n) 
{
  int i = 0;
  int total = 0;
  for(i=0; i<n; i++) 
  {
    total += arr[i];
  }
  return total;
}

int* getPrimes(int n) 
{
  int* result = new int [n];
  result[0] = 2;
  int i = 1;
  int x = 3;
  while(i < n) 
  {
    if(isPrime(x)) 
    {
      result[i] = x;
      i++;
    }
    x += 2;
  }
  return result;
}

bool isPrime(int x) 
{
  if (x < 2)
  {
      return false;
  }
  else if (x == 2)
  {
      return true;
  }
  else if(x % 2 == 0)
  {
    return false;
  }
  for(int i=3; i<= sqrt(x); i+=2) 
  {
    if(x % i == 0) 
    {
      return false;
    }
  }
  return true;
}
